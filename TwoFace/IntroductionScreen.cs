﻿using System;
using System.Collections.Generic;
using System.IO;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    public class IntroductionScreen : ScreenManagerBase
    {
        private IntroductionState _state = IntroductionState.BlackWaiting;
        private long _elapsedtime = 0;
        private int _flashtimer = 0;
        private RectangleShape _flash = new RectangleShape(new Vector2f(600, 500)) { FillColor = Color.White };
        private VertexArray _background = new VertexArray(PrimitiveType.Quads);
        private Player _displayplayer = new Player() { Position = new Vector2f(285, 325) };
        private List<string> _storybackgrounds = new List<string>();
        private int _storyposition = 0;
        private List<Text> _storysectiontexts = new List<Text>();
        private float _alpha = 0;
        private bool _isclosed = false;

        public override event EventHandler<SwitchScreenArgs> SwitchScreen;
        public override event EventHandler CloseThisScreen;

        public IntroductionScreen()
        {
            GenerateBackground();
            StreamReader reader = new StreamReader(".\\Story.txt");
            string section = "";
            while (reader.Peek() != -1)
            {
                string line = reader.ReadLine().Trim();
                if (line == "")
                {
                    _storybackgrounds.Add(section);
                    section = "";
                }
                else
                {
                    if (section != "") section += "\n";
                    section += line;
                }
            }
            if (section != "") _storybackgrounds.Add(section);
            reader.Close();
            reader.Dispose();
        }

        private void GenerateBackground()
        {
            _background.Append(new Vertex(new Vector2f(0, 0), Color.White));
            _background.Append(new Vertex(new Vector2f(600, 0), Color.White));
            _background.Append(new Vertex(new Vector2f(600, 500), Color.White));
            _background.Append(new Vertex(new Vector2f(0, 500), Color.White));
            _background.Append(new Vertex(new Vector2f(5, 5), Color.Black));
            _background.Append(new Vertex(new Vector2f(595, 5), Color.Black));
            _background.Append(new Vertex(new Vector2f(595, 495), Color.Black));
            _background.Append(new Vertex(new Vector2f(5, 495), Color.Black));
            _background.Append(new Vertex(new Vector2f(10, 10), Color.White));
            _background.Append(new Vertex(new Vector2f(590, 10), Color.White));
            _background.Append(new Vertex(new Vector2f(590, 490), Color.White));
            _background.Append(new Vertex(new Vector2f(10, 490), Color.White));
            _background.Append(new Vertex(new Vector2f(130, 15), Color.Black));
            _background.Append(new Vertex(new Vector2f(470, 15), Color.Black));
            _background.Append(new Vertex(new Vector2f(470, 245), Color.Black));
            _background.Append(new Vertex(new Vector2f(130, 245), Color.Black));
            _background.Append(new Vertex(new Vector2f(135, 20), Color.White));
            _background.Append(new Vertex(new Vector2f(465, 20), Color.White));
            _background.Append(new Vertex(new Vector2f(465, 240), Color.White));
            _background.Append(new Vertex(new Vector2f(135, 240), Color.White));
            _background.Append(new Vertex(new Vector2f(225, 275), Color.Black));
            _background.Append(new Vertex(new Vector2f(375, 275), Color.Black));
            _background.Append(new Vertex(new Vector2f(375, 400), Color.Black));
            _background.Append(new Vertex(new Vector2f(225, 400), Color.Black));
            _background.Append(new Vertex(new Vector2f(230, 280), Color.White));
            _background.Append(new Vertex(new Vector2f(370, 280), Color.White));
            _background.Append(new Vertex(new Vector2f(370, 395), Color.White));
            _background.Append(new Vertex(new Vector2f(230, 395), Color.White));
            _background.Append(new Vertex(new Vector2f(301, 275), Color.Black));
            _background.Append(new Vertex(new Vector2f(375, 336.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(375, 338.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(299, 275), Color.Black));
            _background.Append(new Vertex(new Vector2f(299, 275), Color.Black));
            _background.Append(new Vertex(new Vector2f(225, 336.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(225, 338.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(301, 275), Color.Black));
            _background.Append(new Vertex(new Vector2f(301, 400), Color.Black));
            _background.Append(new Vertex(new Vector2f(375, 338.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(375, 336.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(299, 400), Color.Black));
            _background.Append(new Vertex(new Vector2f(299, 400), Color.Black));
            _background.Append(new Vertex(new Vector2f(225, 338.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(225, 336.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(301, 400), Color.Black));


            //_background.Append(new Vertex(new Vector2f(300, 275), Color.Black));
            //_background.Append(new Vertex(new Vector2f(325, 340), Color.Black));
            //_background.Append(new Vertex(new Vector2f(300, 400), Color.Black));
            //_background.Append(new Vertex(new Vector2f(275, 340), Color.Black));

            //_background.Append(new Vertex(new Vector2f(300, 315), Color.Black));
            //_background.Append(new Vertex(new Vector2f(365, 340), Color.Black));
            //_background.Append(new Vertex(new Vector2f(300, 360), Color.Black));
            //_background.Append(new Vertex(new Vector2f(235, 340), Color.Black));

            //_background.Append(new Vertex(new Vector2f(300, 290), Color.White));
            //_background.Append(new Vertex(new Vector2f(315, 340), Color.White));
            //_background.Append(new Vertex(new Vector2f(300, 385), Color.White));
            //_background.Append(new Vertex(new Vector2f(285, 340), Color.White));

            //_background.Append(new Vertex(new Vector2f(300, 325), Color.White));
            //_background.Append(new Vertex(new Vector2f(350, 340), Color.White));
            //_background.Append(new Vertex(new Vector2f(300, 350), Color.White));
            //_background.Append(new Vertex(new Vector2f(250, 340), Color.White));
        }

        private void GetNextStory()
        {
            if (_storyposition != _storybackgrounds.Count)
            {
                _storysectiontexts.Clear();
                string[] splits = _storybackgrounds[_storyposition].Split('\n');
                float top = 25;
                int lineheight = ResourceLoader.MainGameFont.GetLineSpacing(16);
                for (int i = 0; i < splits.Length; i++)
                {
                    Text nwtext = new Text(splits[i], ResourceLoader.MainGameFont) { Color = Color.Black, CharacterSize = 16, Style = Text.Styles.Underlined };
                    nwtext.Position = new Vector2f((float)Math.Round(300.0f - (nwtext.GetLocalBounds().Width / 2.0f), 0), (float)Math.Round(top, 0));
                    _storysectiontexts.Add(nwtext);
                    top += lineheight;
                }
                _storyposition += 1;
            }
            else
            {
                _state = IntroductionState.FadeOut;
                _alpha = 0;
                _flash.FillColor = new Color(0, 0, 0, (byte)_alpha);
            }
        }

        public override void Update(int ElapsedMS, bool IsActiveScreen)
        {
            if (_isclosed && IsActiveScreen)
            {
                CloseThisScreen(this, null);
            }
            _elapsedtime += ElapsedMS;
            if (_state == IntroductionState.BlackWaiting && _elapsedtime > 3000)
            {
                _elapsedtime = 0;
                _state = IntroductionState.Flashing;
            }
            else if (_state == IntroductionState.Flashing && _elapsedtime > 4000)
            {
                _elapsedtime = 0;
                _state = IntroductionState.TextFadeIn;
                GetNextStory();
            }
            else if (_state == IntroductionState.TextSolid && _elapsedtime > 30000)
            {
                _elapsedtime = 0;
                _state = IntroductionState.TextFadeOut;
            }

            if (_state == IntroductionState.Flashing)
            {
                _flashtimer += ElapsedMS;
                if (_flashtimer > 50)
                {
                    _flashtimer -= 50;
                    if (_flash.FillColor.B == Color.White.B)
                    {
                        _flash.FillColor = Color.Black;
                    }
                    else
                    {
                        _flash.FillColor = Color.White;
                    }
                }
            }
            else if (_state == IntroductionState.TextFadeIn)
            {
                _alpha = Math.Min(255, _alpha + (ElapsedMS * 0.2f));
                if (_alpha == 255) { _state = IntroductionState.TextSolid; _elapsedtime = 0; }
                foreach (Text txt in _storysectiontexts) { txt.Color = new Color(0, 0, 0, (byte)_alpha); }
            }
            else if (_state == IntroductionState.TextFadeOut)
            {
                _alpha = Math.Max(0, _alpha - (ElapsedMS * 0.2f));
                if (_alpha == 0) { _state = IntroductionState.TextFadeIn; _elapsedtime = 0; GetNextStory(); }
                foreach (Text txt in _storysectiontexts) { txt.Color = new Color(0, 0, 0, (byte)_alpha); }
            }
            else if (_state == IntroductionState.FadeOut)
            {
                if (IsActiveScreen && !_isclosed)
                {
                    _alpha = Math.Min(255, _alpha + (ElapsedMS * 0.4f));
                    if (_alpha == 255) { GameScreen gmscreen = new GameScreen(); SwitchScreen(this, new SwitchScreenArgs(gmscreen)); _isclosed = true; }
                    _flash.FillColor = new Color(0, 0, 0, (byte)_alpha);
                }
            }
        }

        public override void KeyPressed(object sender, KeyEventArgs e)
        {
            if (e.Code == Keyboard.Key.Space)
            {
                if (_state == IntroductionState.BlackWaiting)
                {
                    _elapsedtime = 0;
                    _state = IntroductionState.Flashing;
                }
                else if (_state == IntroductionState.Flashing)
                {
                    _elapsedtime = 0;
                    _state = IntroductionState.TextFadeIn;
                    GetNextStory();
                }
                else if (_state == IntroductionState.TextFadeIn)
                {
                    _elapsedtime = 0;
                    _alpha = 255;
                    foreach (Text txt in _storysectiontexts) { txt.Color = new Color(0, 0, 0, (byte)_alpha); }
                    _state = IntroductionState.TextSolid;
                }
                else if (_state == IntroductionState.TextSolid)
                {
                    _elapsedtime = 0;
                    _state = IntroductionState.TextFadeOut;
                }
                else if (_state == IntroductionState.TextFadeOut)
                {
                    _elapsedtime = 0;
                    _state = IntroductionState.TextFadeIn;
                    GetNextStory();
                }
            }
        }

        public override void DrawObjects(RenderTarget CurrentRenderSurface, bool IsActiveScreen)
        {
            if (_state == IntroductionState.Flashing)
            {
                CurrentRenderSurface.Draw(_flash);
            }
            else if (_state == IntroductionState.TextFadeIn || _state == IntroductionState.TextSolid || _state == IntroductionState.TextFadeOut)
            {
                CurrentRenderSurface.Draw(_background);
                _displayplayer.Draw(CurrentRenderSurface, Side.LightSide, 0);
                foreach (Text txt in _storysectiontexts) { CurrentRenderSurface.Draw(txt); }
            }
            else if (_state == IntroductionState.FadeOut)
            {
                CurrentRenderSurface.Draw(_background);
                _displayplayer.Draw(CurrentRenderSurface, Side.LightSide, 0);
                CurrentRenderSurface.Draw(_flash);
            }
        }
    }
}
