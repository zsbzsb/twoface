﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    public class Player
    {
        private CircleShape _outsidefeet = new CircleShape(15, 12) { Position = new Vector2f(15, 25) };
        private CircleShape _insidefeet = new CircleShape(13, 8) { Position = new Vector2f(15, 25) };
        private CircleShape _fillfeet = new CircleShape(10, 6) { Position = new Vector2f(15, 25) };
        private VertexArray _outerbody = new VertexArray(PrimitiveType.Quads);
        private VertexArray _innerbody = new VertexArray(PrimitiveType.Quads);
        private RectangleShape _outershaft = new RectangleShape(new Vector2f(3, 35)) { Position = new Vector2f(14, -8) };
        private RectangleShape _innershaft = new RectangleShape(new Vector2f(1, 30)) { Position = new Vector2f(15, -3) };
        private Vector2f _position = new Vector2f(0, 0);
        private float _rotation = 0;

        public Vector2f Position { get { return _position; } set { _position = value; } }

        public Player()
        {
            _outsidefeet.Origin = new Vector2f(_outsidefeet.GetLocalBounds().Width / 2.0f, _outsidefeet.GetLocalBounds().Height / 2.0f);
            _insidefeet.Origin = new Vector2f(_insidefeet.GetLocalBounds().Width / 2.0f, _insidefeet.GetLocalBounds().Height / 2.0f);
            _fillfeet.Origin = new Vector2f(_fillfeet.GetLocalBounds().Width / 2.0f, _fillfeet.GetLocalBounds().Height / 2.0f);
            //Head
            _outerbody.Append(new Vertex(new Vector2f(10, -10)));
            _outerbody.Append(new Vertex(new Vector2f(20, -10)));
            _outerbody.Append(new Vertex(new Vector2f(20, 0)));
            _outerbody.Append(new Vertex(new Vector2f(10, 0)));
            _innerbody.Append(new Vertex(new Vector2f(13, -7)));
            _innerbody.Append(new Vertex(new Vector2f(17, -7)));
            _innerbody.Append(new Vertex(new Vector2f(17, -3)));
            _innerbody.Append(new Vertex(new Vector2f(13, -3)));
            //Body
            _outerbody.Append(new Vertex(new Vector2f(0, 2)));
            _outerbody.Append(new Vertex(new Vector2f(30, 2)));
            _outerbody.Append(new Vertex(new Vector2f(30, 15)));
            _outerbody.Append(new Vertex(new Vector2f(0, 15)));
            _innerbody.Append(new Vertex(new Vector2f(3, 5)));
            _innerbody.Append(new Vertex(new Vector2f(27, 5)));
            _innerbody.Append(new Vertex(new Vector2f(27, 12)));
            _innerbody.Append(new Vertex(new Vector2f(3, 12)));
        }

        public void Draw(RenderTarget DrawTarget, Side CurrentSide, float HeightOffset)
        {
            RenderStates states = RenderStates.Default;
            Transform transform = states.Transform;
            transform.Translate(_position.X, _position.Y + HeightOffset);
            states.Transform = transform;
            if (CurrentSide == Side.LightSide)
            {
                _outsidefeet.FillColor = Color.Black;
                _insidefeet.FillColor = Color.White;
                _fillfeet.FillColor = Color.Black;
                for (uint i = 0; i < _outerbody.VertexCount; i++) { _outerbody[i] = new Vertex(_outerbody[i].Position, Color.Black); }
                for (uint i = 0; i < _innerbody.VertexCount; i++) { _innerbody[i] = new Vertex(_innerbody[i].Position, Color.White); }
                _outershaft.FillColor = Color.Black;
                _innershaft.FillColor = Color.White;
            }
            else if (CurrentSide == Side.DarkSide)
            {
                _outsidefeet.FillColor = Color.White;
                _insidefeet.FillColor = Color.Black;
                _fillfeet.FillColor = Color.White;
                for (uint i = 0; i < _outerbody.VertexCount; i++) { _outerbody[i] = new Vertex(_outerbody[i].Position, Color.White); }
                for (uint i = 0; i < _innerbody.VertexCount; i++) { _innerbody[i] = new Vertex(_innerbody[i].Position, Color.Black); }
                _outershaft.FillColor = Color.White;
                _innershaft.FillColor = Color.Black;
            }
            DrawTarget.Draw(_outsidefeet, states);
            DrawTarget.Draw(_insidefeet, states);
            DrawTarget.Draw(_fillfeet, states);
            DrawTarget.Draw(_outershaft, states);
            DrawTarget.Draw(_innershaft, states);
            DrawTarget.Draw(_outerbody, states);
            DrawTarget.Draw(_innerbody, states);
        }
        public void Rotate(float RotateAmount)
        {
            _rotation += RotateAmount;
            while (_rotation >= 360) _rotation -= 360;
            while (_rotation <= -360) _rotation += 360;
            _outsidefeet.Rotation = _rotation;
            _insidefeet.Rotation = _rotation;
            _fillfeet.Rotation = _rotation;
        }
    }
}
