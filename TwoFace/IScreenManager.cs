﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    public interface IScreenManager
    {
        void Update(int ElapsedMS, bool IsActiveScreen);
        void UpdateObjects(int ElapsedMS, bool IsActiveScreen);
        void UpdateFrames(int ElapsedMS, bool IsActiveScreen);
        void DrawObjects(SFML.Graphics.RenderTarget CurrentRenderSurface, bool IsActiveScreen);
        void KeyPressed(object sender, KeyEventArgs e);
        void KeyReleased(object sender, KeyEventArgs e);
        void MouseMove(object sender, MouseMoveEventArgs e);
        void MouseDown(object sender, MouseButtonEventArgs e);
        void MouseUp(object sender, MouseButtonEventArgs e);
        event EventHandler CloseThisScreen;
        event EventHandler<SwitchScreenArgs> SwitchScreen;
        bool DrawWhenInactive { get; }
    }
}
