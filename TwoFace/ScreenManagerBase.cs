﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    public class ScreenManagerBase : IScreenManager
    {
        public virtual void Update(int ElapsedMS, bool IsActiveScreen) { }
        public virtual void UpdateObjects(int ElapsedMS, bool IsActiveScreen) { }
        public virtual void UpdateFrames(int ElapsedMS, bool IsActiveScreen) { }
        public virtual void DrawObjects(SFML.Graphics.RenderTarget CurrentRenderSurface, bool IsActiveScreen) { }
        public virtual void KeyPressed(object sender, KeyEventArgs e) { }
        public virtual void KeyReleased(object sender, KeyEventArgs e) { }
        public virtual void MouseMove(object sender, MouseMoveEventArgs e) { }
        public virtual void MouseDown(object sender, MouseButtonEventArgs e) { }
        public virtual void MouseUp(object sender, MouseButtonEventArgs e) { }
        public virtual event EventHandler CloseThisScreen;
        public virtual event EventHandler<SwitchScreenArgs> SwitchScreen;
        public virtual bool DrawWhenInactive { get { return false; } }
    }
}
