﻿using System;

namespace TwoFace
{
    public class SwitchScreenArgs : EventArgs
    {
        internal IScreenManager CurrentScreenManager = null;
        internal SwitchScreenArgs(IScreenManager NewScreenManager)
        {
            CurrentScreenManager = NewScreenManager;
        }
    }
}
