﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    public class Orb
    {
        private CircleShape _outercircle = new CircleShape(6, 24) { Position = new Vector2f(6, 6) };
        private CircleShape _innercircle = new CircleShape(4, 16) { Position = new Vector2f(6, 6) };
        private CircleShape _fillcircle = new CircleShape(2, 8) { Position = new Vector2f(6, 6) };
        private Vector2f _position = new Vector2f(0, 0);
        private float _livetime = 0;
        private Vector2f _movevelocity = new Vector2f(0, 0);

        public Vector2f Position { get { return _position; } set { _position = value; } }
        public float LiveTime { get { return _livetime; } set { _livetime = value; } }
        public Vector2f MoveVelocity { get { return _movevelocity; } set { _movevelocity = value; } }

        public Orb()
        {
            _outercircle.Origin = new Vector2f(_outercircle.GetLocalBounds().Width / 2.0f, _outercircle.GetLocalBounds().Height / 2.0f);
            _innercircle.Origin = new Vector2f(_innercircle.GetLocalBounds().Width / 2.0f, _innercircle.GetLocalBounds().Height / 2.0f);
            _fillcircle.Origin = new Vector2f(_fillcircle.GetLocalBounds().Width / 2.0f, _fillcircle.GetLocalBounds().Height / 2.0f);
        }

        public void Draw(RenderTarget DrawTarget, Side CurrentSide, float HeightOffset)
        {
            RenderStates states = RenderStates.Default;
            Transform transform = states.Transform;
            transform.Translate(_position.X, _position.Y + HeightOffset);
            states.Transform = transform;
            if (CurrentSide == Side.LightSide)
            {
                _outercircle.FillColor = Color.Black;
                _innercircle.FillColor = Color.White;
                _fillcircle.FillColor = Color.Black;
            }
            else if (CurrentSide == Side.DarkSide)
            {
                _outercircle.FillColor = Color.White;
                _innercircle.FillColor = Color.Black;
                _fillcircle.FillColor = Color.White;
            }
            DrawTarget.Draw(_outercircle, states);
            DrawTarget.Draw(_innercircle, states);
            DrawTarget.Draw(_fillcircle, states);
        }
    }
}
