﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    public class MainMenu : ScreenManagerBase
    {
        private Sprite _logosprite = new Sprite(ResourceLoader.SelfLogo) { Scale = new Vector2f(0.1875f, 0.1875f), Position = new Vector2f(450, 350) };
        private VertexArray _background = new VertexArray(PrimitiveType.Triangles);
        private MainMenuButton _newgamebutton = new MainMenuButton() { DisplayText = "New Game", Position = new Vector2f(225, 200) };
        private Text _namept1 = new Text("Two", ResourceLoader.GameLogoFont) { Color = Color.Black, CharacterSize = 64, Position = new Vector2f(130, 10), Rotation = 20 };
        private Text _namept2 = new Text("Face", ResourceLoader.GameLogoFont) { Color = Color.White, CharacterSize = 64, Position = new Vector2f(250, 10), Rotation = 20 };
        private Text _writtenby = new Text("Written By", ResourceLoader.MainGameFont) { Color = Color.White, CharacterSize = 14, Position = new Vector2f(470, 325), Rotation = 30 };
        private Text _zachbrown = new Text("Zachariah Brown", ResourceLoader.MainGameFont) { Color = Color.Black, CharacterSize = 14, Position = new Vector2f(390, 420), Rotation = 25 };
        private bool _fadeout = false;
        private RectangleShape _fadeoutcover = new RectangleShape(new Vector2f(600, 500)) { FillColor = new Color(0, 0, 0, 0) };
        private float _fadealpha = 0;

        public override event EventHandler<SwitchScreenArgs> SwitchScreen;

        public MainMenu()
        {
            CreateBackground();
        }

        private void CreateBackground()
        {
            _background.Append(new Vertex(new Vector2f(300, 250), Color.White));
            _background.Append(new Vertex(new Vector2f(0, 0), Color.White));
            _background.Append(new Vertex(new Vector2f(200, 0), Color.White));
            _background.Append(new Vertex(new Vector2f(300, 250), Color.White));
            _background.Append(new Vertex(new Vector2f(400, 0), Color.White));
            _background.Append(new Vertex(new Vector2f(600, 0), Color.White));
            _background.Append(new Vertex(new Vector2f(300, 250), Color.White));
            _background.Append(new Vertex(new Vector2f(600, 166.66f), Color.White));
            _background.Append(new Vertex(new Vector2f(600, 333.33f), Color.White));
            _background.Append(new Vertex(new Vector2f(300, 250), Color.White));
            _background.Append(new Vertex(new Vector2f(400, 500), Color.White));
            _background.Append(new Vertex(new Vector2f(600, 500), Color.White));
            _background.Append(new Vertex(new Vector2f(300, 250), Color.White));
            _background.Append(new Vertex(new Vector2f(0, 500), Color.White));
            _background.Append(new Vertex(new Vector2f(200, 500), Color.White));
            _background.Append(new Vertex(new Vector2f(300, 250), Color.White));
            _background.Append(new Vertex(new Vector2f(0, 166.66f), Color.White));
            _background.Append(new Vertex(new Vector2f(0, 333.33f), Color.White));
        }

        public override void DrawObjects(RenderTarget CurrentRenderSurface, bool IsActiveScreen)
        {
            CurrentRenderSurface.Draw(_background);
            CurrentRenderSurface.Draw(_logosprite);
            CurrentRenderSurface.Draw(_namept1);
            CurrentRenderSurface.Draw(_namept2);
            CurrentRenderSurface.Draw(_writtenby);
            CurrentRenderSurface.Draw(_zachbrown);
            if (!_fadeout)
            {
                _newgamebutton.Draw(CurrentRenderSurface);
            }
            else
            {
                CurrentRenderSurface.Draw(_fadeoutcover);
            }
        }

        public override void Update(int ElapsedMS, bool IsActiveScreen)
        {
            if (IsActiveScreen)
            {
                if (_fadeout && _fadealpha < 255)
                {
                    _fadealpha += (float)ElapsedMS / 3.0f;
                    _fadeoutcover.FillColor = new Color(0, 0, 0, (byte)_fadealpha);
                }
                else if (_fadeout && _fadealpha >= 255)
                {
                    _fadeout = false;
                    _fadealpha = 0;
                    IntroductionScreen intro = new IntroductionScreen();
                    SwitchScreen(this, new SwitchScreenArgs(intro));
                }
            }
        }

        public override void MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!_fadeout)
            {
                if (_newgamebutton.PointinButton(new Vector2i(e.X, e.Y)))
                {
                    _fadeout = true;
                }
            }
        }
    }
}
