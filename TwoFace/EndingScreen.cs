﻿using System;
using System.Collections.Generic;
using System.IO;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    public class EndingScreen : ScreenManagerBase
    {
        private EndState _state = EndState.BlackWaiting;
        private long _elapsedtime = 0;
        private int _flashtimer = 0;
        private RectangleShape _flash = new RectangleShape(new Vector2f(600, 500)) { FillColor = Color.White };
        private VertexArray _background = new VertexArray(PrimitiveType.Quads);
        private Player _displayplayer = new Player() { Position = new Vector2f(285, 325) };
        private List<string> _storybackgrounds = new List<string>();
        private int _storyposition = 0;
        private List<Text> _storysectiontexts = new List<Text>();
        private float _alpha = 0;
        private MainMenuButton _yesbutton = new MainMenuButton() { DisplayText = "Yes", Position = new Vector2f(140, 400) };
        private MainMenuButton _nobutton = new MainMenuButton() { DisplayText = "No", Position = new Vector2f(310, 400) };
        private Text _iam = new Text("I am Mandy  The Great and Powerful", ResourceLoader.MainGameFont, 35) { Position = new Vector2f(15, 20), Color = Color.Red };

        public override event EventHandler CloseThisScreen;

        public EndingScreen()
        {
            GenerateBackground();
            StreamReader reader = new StreamReader(".\\Ending.txt");
            string section = "";
            while (reader.Peek() != -1)
            {
                string line = reader.ReadLine().Trim();
                if (line == "")
                {
                    _storybackgrounds.Add(section);
                    section = "";
                }
                else
                {
                    if (section != "") section += "\n";
                    section += line;
                }
            }
            if (section != "") _storybackgrounds.Add(section);
            reader.Close();
            reader.Dispose();
        }

        private void GenerateBackground()
        {
            _background.Append(new Vertex(new Vector2f(0, 0), Color.White));
            _background.Append(new Vertex(new Vector2f(600, 0), Color.White));
            _background.Append(new Vertex(new Vector2f(600, 500), Color.White));
            _background.Append(new Vertex(new Vector2f(0, 500), Color.White));
            _background.Append(new Vertex(new Vector2f(5, 5), Color.Black));
            _background.Append(new Vertex(new Vector2f(595, 5), Color.Black));
            _background.Append(new Vertex(new Vector2f(595, 495), Color.Black));
            _background.Append(new Vertex(new Vector2f(5, 495), Color.Black));
            _background.Append(new Vertex(new Vector2f(10, 10), Color.White));
            _background.Append(new Vertex(new Vector2f(590, 10), Color.White));
            _background.Append(new Vertex(new Vector2f(590, 490), Color.White));
            _background.Append(new Vertex(new Vector2f(10, 490), Color.White));
            _background.Append(new Vertex(new Vector2f(130, 15), Color.Black));
            _background.Append(new Vertex(new Vector2f(470, 15), Color.Black));
            _background.Append(new Vertex(new Vector2f(470, 245), Color.Black));
            _background.Append(new Vertex(new Vector2f(130, 245), Color.Black));
            _background.Append(new Vertex(new Vector2f(135, 20), Color.White));
            _background.Append(new Vertex(new Vector2f(465, 20), Color.White));
            _background.Append(new Vertex(new Vector2f(465, 240), Color.White));
            _background.Append(new Vertex(new Vector2f(135, 240), Color.White));
            _background.Append(new Vertex(new Vector2f(225, 275), Color.Black));
            _background.Append(new Vertex(new Vector2f(375, 275), Color.Black));
            _background.Append(new Vertex(new Vector2f(375, 400), Color.Black));
            _background.Append(new Vertex(new Vector2f(225, 400), Color.Black));
            _background.Append(new Vertex(new Vector2f(230, 280), Color.White));
            _background.Append(new Vertex(new Vector2f(370, 280), Color.White));
            _background.Append(new Vertex(new Vector2f(370, 395), Color.White));
            _background.Append(new Vertex(new Vector2f(230, 395), Color.White));
            _background.Append(new Vertex(new Vector2f(301, 275), Color.Black));
            _background.Append(new Vertex(new Vector2f(375, 336.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(375, 338.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(299, 275), Color.Black));
            _background.Append(new Vertex(new Vector2f(299, 275), Color.Black));
            _background.Append(new Vertex(new Vector2f(225, 336.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(225, 338.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(301, 275), Color.Black));
            _background.Append(new Vertex(new Vector2f(301, 400), Color.Black));
            _background.Append(new Vertex(new Vector2f(375, 338.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(375, 336.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(299, 400), Color.Black));
            _background.Append(new Vertex(new Vector2f(299, 400), Color.Black));
            _background.Append(new Vertex(new Vector2f(225, 338.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(225, 336.5f), Color.Black));
            _background.Append(new Vertex(new Vector2f(301, 400), Color.Black));
        }

        private void GetNextStory()
        {
            if (_storyposition != _storybackgrounds.Count)
            {
                _storysectiontexts.Clear();
                string[] splits = _storybackgrounds[_storyposition].Split('\n');
                float top = 25;
                int lineheight = ResourceLoader.MainGameFont.GetLineSpacing(16);
                for (int i = 0; i < splits.Length; i++)
                {
                    Text nwtext = new Text(splits[i], ResourceLoader.MainGameFont) { Color = Color.Black, CharacterSize = 16, Style = Text.Styles.Underlined };
                    nwtext.Position = new Vector2f((float)Math.Round(300.0f - (nwtext.GetLocalBounds().Width / 2.0f), 0), (float)Math.Round(top, 0));
                    _storysectiontexts.Add(nwtext);
                    top += lineheight;
                }
                _storyposition += 1;
            }
        }

        public override void Update(int ElapsedMS, bool IsActiveScreen)
        {
            _elapsedtime += ElapsedMS;
            if (_state == EndState.BlackWaiting && _elapsedtime > 3000)
            {
                _elapsedtime = 0;
                _state = EndState.Flashing;
            }
            else if (_state == EndState.Flashing && _elapsedtime > 4000)
            {
                _elapsedtime = 0;
                _state = EndState.TextFadeIn;
                GetNextStory();
            }
            else if (_state == EndState.TextSolid && _elapsedtime > 30000)
            {
                _elapsedtime = 0;
                if (_storybackgrounds.Count > _storyposition)
                {
                    _state = EndState.TextFadeOut;
                }
            }

            if (_state == EndState.Flashing)
            {
                _flashtimer += ElapsedMS;
                if (_flashtimer > 50)
                {
                    _flashtimer -= 50;
                    if (_flash.FillColor.B == Color.White.B)
                    {
                        _flash.FillColor = Color.Black;
                    }
                    else
                    {
                        _flash.FillColor = Color.White;
                    }
                }
            }
            else if (_state == EndState.TextFadeIn)
            {
                _alpha = Math.Min(255, _alpha + (ElapsedMS * 0.2f));
                if (_alpha == 255) { _state = EndState.TextSolid; _elapsedtime = 0; }
                foreach (Text txt in _storysectiontexts) { txt.Color = new Color(0, 0, 0, (byte)_alpha); }
            }
            else if (_state == EndState.TextFadeOut)
            {
                _alpha = Math.Max(0, _alpha - (ElapsedMS * 0.2f));
                if (_alpha == 0) { _state = EndState.TextFadeIn; _elapsedtime = 0; GetNextStory(); }
                foreach (Text txt in _storysectiontexts) { txt.Color = new Color(0, 0, 0, (byte)_alpha); }
            }
        }

        public override void DrawObjects(RenderTarget CurrentRenderSurface, bool IsActiveScreen)
        {
            if (_state == EndState.Flashing)
            {
                CurrentRenderSurface.Draw(_flash);
            }
            else if (_state == EndState.TextFadeIn || _state == EndState.TextSolid || _state == EndState.TextFadeOut)
            {
                CurrentRenderSurface.Draw(_background);
                _displayplayer.Draw(CurrentRenderSurface, Side.LightSide, 0);
                foreach (Text txt in _storysectiontexts) { CurrentRenderSurface.Draw(txt); }
            }
            if ((_state == EndState.TextFadeIn || _state == EndState.TextSolid || _state == EndState.TextFadeOut) && _storybackgrounds.Count <= _storyposition)
            {
                _yesbutton.Draw(CurrentRenderSurface);
                _nobutton.Draw(CurrentRenderSurface);
            }
            else if (_state == EndState.DogShow)
            {
                CurrentRenderSurface.Draw(new Sprite(ResourceLoader.Mandy));
                CurrentRenderSurface.Draw(_iam);
            }
        }

        public override void KeyReleased(object sender, KeyEventArgs e)
        {
            if (_state == EndState.DogShow && e.Code == Keyboard.Key.Space)
            {
                CloseThisScreen(this, null);
            }
        }

        public override void MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_storybackgrounds.Count <= _storyposition)
            {
                if (_yesbutton.PointinButton(new Vector2i(e.X, e.Y)))
                {
                    _state = EndState.DogShow;
                }
                else if (_nobutton.PointinButton(new Vector2i(e.X, e.Y)))
                {
                    CloseThisScreen(this, null);
                }
            }
        }
    }
}
