﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    public class SpikeBall : IEnity
    {
        private VertexArray _spikes = new VertexArray(PrimitiveType.Lines);
        private Vector2f _position = new Vector2f(0, 0);

        public Vector2f Position { get { return _position; } set { _position = value; } }
        public FloatRect BoundingBox { get { return new FloatRect(_position.X, _position.Y, 25, 15); } }

        public SpikeBall()
        {
            _spikes.Append(new Vertex(new Vector2f(12.5f, 0)));
            _spikes.Append(new Vertex(new Vector2f(12.5f, 15)));
            _spikes.Append(new Vertex(new Vector2f(5, 7.5f)));
            _spikes.Append(new Vertex(new Vector2f(20, 7.5f)));
            _spikes.Append(new Vertex(new Vector2f(5, 0)));
            _spikes.Append(new Vertex(new Vector2f(20, 15)));
            _spikes.Append(new Vertex(new Vector2f(20, 0)));
            _spikes.Append(new Vertex(new Vector2f(5, 15)));
        }

        public void Draw(RenderTarget DrawTarget, Side CurrentSide, float HeightOffset)
        {
            RenderStates states = RenderStates.Default;
            Transform transform = states.Transform;
            transform.Translate(_position.X, _position.Y + HeightOffset);
            states.Transform = transform;
            if (CurrentSide == Side.LightSide)
            {
                for (uint i = 0; i < _spikes.VertexCount; i++) { _spikes[i] = new Vertex(_spikes[i].Position, Color.Black); }
            }
            else if (CurrentSide == Side.DarkSide)
            {
                for (uint i = 0; i < _spikes.VertexCount; i++) { _spikes[i] = new Vertex(_spikes[i].Position, Color.White); }
            }
            DrawTarget.Draw(_spikes, states);
        }
    }
}
