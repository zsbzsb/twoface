﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    class Program
    {
        static void Main(string[] args)
        {
            new MainLoop().Run();
        }
    }
}
