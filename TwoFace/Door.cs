﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    public class Door
    {
        private VertexArray _outerdoor = new VertexArray(PrimitiveType.Quads);
        private VertexArray _fill = new VertexArray(PrimitiveType.Quads);
        private VertexArray _details = new VertexArray(PrimitiveType.Quads);
        private Vector2f _position = new Vector2f(0, 0);

        public Vector2f Position { get { return _position; } set { _position = value; } }
        public FloatRect BoundingBox { get { return new FloatRect(_position.X, _position.Y, 50, 60); } }

        public Door(Colors CurrentColor)
        {
            Color clr = new Color(0, 0, 0);
            if (CurrentColor == Colors.Red)
            {
                clr = new Color(255, 0, 0);
            }
            else if (CurrentColor == Colors.Orange)
            {
                clr = new Color(255, 150, 0);
            }
            else if (CurrentColor == Colors.Yellow)
            {
                clr = new Color(255, 255, 0);
            }
            else if (CurrentColor == Colors.Green)
            {
                clr = new Color(0, 255, 0);
            }
            else if (CurrentColor == Colors.LightBlue)
            {
                clr = new Color(0, 255, 255);
            }
            else if (CurrentColor == Colors.DarkBlue)
            {
                clr = new Color(0, 0, 255);
            }
            else if (CurrentColor == Colors.Purple)
            {
                clr = new Color(255, 0, 255);
            }
            _outerdoor.Append(new Vertex(new Vector2f(0, 0)));
            _outerdoor.Append(new Vertex(new Vector2f(50, 0)));
            _outerdoor.Append(new Vertex(new Vector2f(50, 60)));
            _outerdoor.Append(new Vertex(new Vector2f(0, 60)));
            _fill.Append(new Vertex(new Vector2f(2, 2), clr));
            _fill.Append(new Vertex(new Vector2f(48, 2), clr));
            _fill.Append(new Vertex(new Vector2f(48, 58), clr));
            _fill.Append(new Vertex(new Vector2f(2, 58), clr));
            _details.Append(new Vertex(new Vector2f(23, 0)));
            _details.Append(new Vertex(new Vector2f(27, 0)));
            _details.Append(new Vertex(new Vector2f(27, 60)));
            _details.Append(new Vertex(new Vector2f(23, 60)));
            _details.Append(new Vertex(new Vector2f(8, 0)));
            _details.Append(new Vertex(new Vector2f(9, 0)));
            _details.Append(new Vertex(new Vector2f(9, 60)));
            _details.Append(new Vertex(new Vector2f(8, 60)));
            _details.Append(new Vertex(new Vector2f(16, 0)));
            _details.Append(new Vertex(new Vector2f(17, 0)));
            _details.Append(new Vertex(new Vector2f(17, 60)));
            _details.Append(new Vertex(new Vector2f(16, 60)));
            _details.Append(new Vertex(new Vector2f(33, 0)));
            _details.Append(new Vertex(new Vector2f(34, 0)));
            _details.Append(new Vertex(new Vector2f(34, 60)));
            _details.Append(new Vertex(new Vector2f(33, 60)));
            _details.Append(new Vertex(new Vector2f(41, 0)));
            _details.Append(new Vertex(new Vector2f(42, 0)));
            _details.Append(new Vertex(new Vector2f(42, 60)));
            _details.Append(new Vertex(new Vector2f(41, 60)));
            _details.Append(new Vertex(new Vector2f(20, 25)));
            _details.Append(new Vertex(new Vector2f(22, 25)));
            _details.Append(new Vertex(new Vector2f(22, 30)));
            _details.Append(new Vertex(new Vector2f(20, 30)));
            _details.Append(new Vertex(new Vector2f(28, 25)));
            _details.Append(new Vertex(new Vector2f(30, 25)));
            _details.Append(new Vertex(new Vector2f(30, 30)));
            _details.Append(new Vertex(new Vector2f(28, 30)));

        }

        public void Draw(RenderTarget DrawTarget, Side CurrentSide, float HeightOffset)
        {
            RenderStates states = RenderStates.Default;
            Transform transform = states.Transform;
            transform.Translate(_position.X, _position.Y + HeightOffset);
            states.Transform = transform;
            if (CurrentSide == Side.LightSide)
            {
                for (uint i = 0; i < _outerdoor.VertexCount; i++) { _outerdoor[i] = new Vertex(_outerdoor[i].Position, Color.Black); }
                for (uint i = 0; i < _details.VertexCount; i++) { _details[i] = new Vertex(_details[i].Position, Color.Black); }
            }
            else if (CurrentSide == Side.DarkSide)
            {
                for (uint i = 0; i < _outerdoor.VertexCount; i++) { _outerdoor[i] = new Vertex(_outerdoor[i].Position, Color.White); }
                for (uint i = 0; i < _details.VertexCount; i++) { _details[i] = new Vertex(_details[i].Position, Color.White); }
            }
            DrawTarget.Draw(_outerdoor, states);
            DrawTarget.Draw(_fill, states);
            DrawTarget.Draw(_details, states);
        }
    }
}
