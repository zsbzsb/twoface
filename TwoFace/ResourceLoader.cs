﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    public static class ResourceLoader
    {
        private static Texture _selflogo = null;
        public static Texture SelfLogo { get { return _selflogo; } }
        private static Texture _mandy = null;
        public static Texture Mandy { get { return _mandy; } }
        private static Font _gamelogofont = null;
        public static Font GameLogoFont { get { return _gamelogofont; } }
        private static Font _maingamefont = null;
        public static Font MainGameFont { get { return _maingamefont; } }

        public static void LoadResources()
        {
            _selflogo = new Texture(".\\zsb.png");
            _mandy = new Texture(".\\dog.png");
            _gamelogofont = new Font(".\\Facet Ultra.ttf");
            _maingamefont = new Font(".\\Facet Black.ttf");
        }
    }
}
