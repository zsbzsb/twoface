﻿using System;
using SFML.Window;
using SFML.Graphics;
namespace TwoFace
{
    public interface IEnity
    {
        Vector2f Position { get; set; }
        FloatRect BoundingBox { get; }
        void Draw(RenderTarget DrawTarget, Side CurrentSide, float HeightOffset);
    }
}
