﻿using System;
using System.Collections.Generic;
using System.IO;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    public class GameScreen : ScreenManagerBase
    {
        private GameState _state = GameState.Playing;
        private VertexArray _hudbackground = new VertexArray(PrimitiveType.Quads);
        private Text _hudtext = new Text("", ResourceLoader.MainGameFont) { Position = new Vector2f(300, 130), CharacterSize = 32, Color = Color.White };
        private VertexArray _borders = new VertexArray(PrimitiveType.Quads);
        private Player _currentplayer = new Player() { Position = new Vector2f(150, 300) };
        private RenderTexture _darkside = new RenderTexture(275, 480);
        private RenderTexture _lightside = new RenderTexture(275, 480);
        private RenderTexture _darkmask = new RenderTexture(275, 480);
        private RenderTexture _lightmask = new RenderTexture(275, 480);
        private Texture _darkvisibility = null;
        private Texture _lightvisibility = null;
        private bool _moveup = false;
        private bool _moveright = false;
        private bool _moveleft = false;
        private bool _movedown = false;
        private int _jumpremaining = 300;
        private Vector2f _movevelocity = new Vector2f(0, 0);
        private int _level = 0;
        private List<SolidBlock> _allsolidblocklist = new List<SolidBlock>();
        private List<PlatformBlock> _allplatformblocklist = new List<PlatformBlock>();
        private List<SolidBlock> _sharedsolidblocklist = new List<SolidBlock>();
        private List<PlatformBlock> _sharedplatformblocklist = new List<PlatformBlock>();
        private List<SolidBlock> _darksolidblocklist = new List<SolidBlock>();
        private List<PlatformBlock> _darkplatformblocklist = new List<PlatformBlock>();
        private List<SolidBlock> _lightsolidblocklist = new List<SolidBlock>();
        private List<PlatformBlock> _lightplatformblocklist = new List<PlatformBlock>();
        private List<Orb> _darksideorbs = new List<Orb>();
        private List<Orb> _lightsideorbs = new List<Orb>();
        private VertexArray _hud = new VertexArray(PrimitiveType.Quads);
        private RectangleShape _darkorbsremainingbar = new RectangleShape(new Vector2f(100, 15)) { FillColor = Color.White, Position = new Vector2f(145, 20) };
        private int _darkorbsremaining = 10;
        private RectangleShape _lightorbsremainingbar = new RectangleShape(new Vector2f(100, 15)) { FillColor = Color.Black, Position = new Vector2f(445, 20) };
        private int _cooldown = 1000;
        private RectangleShape _darkcooldownbar = new RectangleShape(new Vector2f(100, 3)) { FillColor = Color.White, Position = new Vector2f(145, 36) };
        private RectangleShape _lightcooldownbar = new RectangleShape(new Vector2f(100, 3)) { FillColor = Color.Black, Position = new Vector2f(445, 36) };
        private Text _darkpower = new Text("Dark Power", ResourceLoader.MainGameFont) { Position = new Vector2f(55, 20), CharacterSize = 16, Color = Color.White };
        private Text _lightpower = new Text("Light Power", ResourceLoader.MainGameFont) { Position = new Vector2f(355, 20), CharacterSize = 16, Color = Color.Black };
        private int _lightorbsremaining = 10;
        private Dictionary<Door, int> _alldoormaplist = new Dictionary<Door, int>();
        private Dictionary<Door, int> _shareddoormaplist = new Dictionary<Door, int>();
        private Dictionary<Door, int> _darkdoormaplist = new Dictionary<Door, int>();
        private Dictionary<Door, int> _lightdoormaplist = new Dictionary<Door, int>();
        private List<IEnity> _allenitylist = new List<IEnity>();
        private List<IEnity> _sharedenitylist = new List<IEnity>();
        private List<IEnity> _darkenitylist = new List<IEnity>();
        private List<IEnity> _lightenitylist = new List<IEnity>();
        private RectangleShape _fadecover = new RectangleShape(new Vector2f(600, 500)) { FillColor = new Color(0, 0, 0, 0) };
        private float _fadealpha = 0;
        private bool _isclosed = false;

        public override event EventHandler<SwitchScreenArgs> SwitchScreen;
        public override event EventHandler CloseThisScreen;

        public GameScreen()
        {
            CreateBorders();
            LoadLevel();
            GeneratePlayerVisibleArea();
            GenerateHud();
        }

        private void GenerateHud()
        {
            _hud.Append(new Vertex(new Vector2f(50, 15), Color.White));
            _hud.Append(new Vertex(new Vector2f(250, 15), Color.White));
            _hud.Append(new Vertex(new Vector2f(250, 45), Color.White));
            _hud.Append(new Vertex(new Vector2f(50, 45), Color.White));
            _hud.Append(new Vertex(new Vector2f(52, 17), Color.Black));
            _hud.Append(new Vertex(new Vector2f(248, 17), Color.Black));
            _hud.Append(new Vertex(new Vector2f(248, 43), Color.Black));
            _hud.Append(new Vertex(new Vector2f(52, 43), Color.Black));
            _hud.Append(new Vertex(new Vector2f(350, 15), Color.Black));
            _hud.Append(new Vertex(new Vector2f(550, 15), Color.Black));
            _hud.Append(new Vertex(new Vector2f(550, 45), Color.Black));
            _hud.Append(new Vertex(new Vector2f(350, 45), Color.Black));
            _hud.Append(new Vertex(new Vector2f(352, 17), Color.White));
            _hud.Append(new Vertex(new Vector2f(548, 17), Color.White));
            _hud.Append(new Vertex(new Vector2f(548, 43), Color.White));
            _hud.Append(new Vertex(new Vector2f(352, 43), Color.White));
            _hudbackground.Append(new Vertex(new Vector2f(200, 100), Color.White));
            _hudbackground.Append(new Vertex(new Vector2f(400, 100), Color.White));
            _hudbackground.Append(new Vertex(new Vector2f(400, 200), Color.White));
            _hudbackground.Append(new Vertex(new Vector2f(200, 200), Color.White));
            _hudbackground.Append(new Vertex(new Vector2f(205, 105), Color.Black));
            _hudbackground.Append(new Vertex(new Vector2f(395, 105), Color.Black));
            _hudbackground.Append(new Vertex(new Vector2f(395, 195), Color.Black));
            _hudbackground.Append(new Vertex(new Vector2f(205, 195), Color.Black));
            _hudbackground.Append(new Vertex(new Vector2f(210, 110), Color.White));
            _hudbackground.Append(new Vertex(new Vector2f(390, 110), Color.White));
            _hudbackground.Append(new Vertex(new Vector2f(390, 190), Color.White));
            _hudbackground.Append(new Vertex(new Vector2f(210, 190), Color.White));
            _hudbackground.Append(new Vertex(new Vector2f(215, 115), Color.Black));
            _hudbackground.Append(new Vertex(new Vector2f(385, 115), Color.Black));
            _hudbackground.Append(new Vertex(new Vector2f(385, 185), Color.Black));
            _hudbackground.Append(new Vertex(new Vector2f(215, 185), Color.Black));
        }

        private void GeneratePlayerVisibleArea()
        {
            RenderTexture vistext = new RenderTexture(512, 512);
            vistext.Clear(Color.Transparent);
            vistext.Draw(new CircleShape(256, 360) { FillColor = Color.Transparent, OutlineColor = Color.Black, OutlineThickness = 1000 }, new RenderStates(BlendMode.None));
            vistext.Display();
            _darkvisibility = new Texture(vistext.Texture);
            vistext.Clear(Color.Transparent);
            vistext.Draw(new CircleShape(256, 360) { FillColor = Color.Transparent, OutlineColor = Color.White, OutlineThickness = 1000 }, new RenderStates(BlendMode.None));
            vistext.Display();
            _lightvisibility = new Texture(vistext.Texture);
        }

        private void LoadLevel()
        {
            _jumpremaining = 300;
            _movevelocity = new Vector2f(0, 0);
             _moveup = false;
             _moveright = false;
             _moveleft = false;
             _darkorbsremaining = 10;
             _lightorbsremaining = 10;
             _cooldown = 1000;
             _darksideorbs.Clear();
             _lightsideorbs.Clear();
            _allsolidblocklist.Clear();
            _allplatformblocklist.Clear();
            _sharedsolidblocklist.Clear();
            _sharedplatformblocklist.Clear();
            _darksolidblocklist.Clear();
            _darkplatformblocklist.Clear();
            _lightsolidblocklist.Clear();
            _lightplatformblocklist.Clear();
            _alldoormaplist.Clear();
            _shareddoormaplist.Clear();
            _darkdoormaplist.Clear();
            _lightdoormaplist.Clear();
            _allenitylist.Clear();
            _sharedenitylist.Clear();
            _darkenitylist.Clear();
            _lightenitylist.Clear();
            if (File.Exists(".\\Levels\\" + _level.ToString() + ".lvl"))
            {
                int ypos = 0;
                StreamReader reader = new StreamReader(".\\Levels\\" + _level.ToString() + ".lvl");
                while (reader.Peek() != -1)
                {
                    string line = reader.ReadLine();
                    int xpos = 0;
                    foreach (char chr in line.ToCharArray())
                    {
                        if (chr == '.')
                        {
                        }
                        else if (chr == '#')
                        {
                            SolidBlock newblock = new SolidBlock() { Position = new Vector2f(xpos, ypos) };
                            _allsolidblocklist.Add(newblock);
                            _sharedsolidblocklist.Add(newblock);
                        }
                        else if (chr == '-')
                        {
                            PlatformBlock newblock = new PlatformBlock() { Position = new Vector2f(xpos, ypos) };
                            _allplatformblocklist.Add(newblock);
                            _sharedplatformblocklist.Add(newblock);
                        }
                        else if (chr == 'D')
                        {
                            SolidBlock newblock = new SolidBlock() { Position = new Vector2f(xpos, ypos) };
                            _allsolidblocklist.Add(newblock);
                            _darksolidblocklist.Add(newblock);
                        }
                        else if (chr == 'd')
                        {
                            PlatformBlock newblock = new PlatformBlock() { Position = new Vector2f(xpos, ypos) };
                            _allplatformblocklist.Add(newblock);
                            _darkplatformblocklist.Add(newblock);
                        }
                        else if (chr == 'L')
                        {
                            SolidBlock newblock = new SolidBlock() { Position = new Vector2f(xpos, ypos) };
                            _allsolidblocklist.Add(newblock);
                            _lightsolidblocklist.Add(newblock);
                        }
                        else if (chr == 'l')
                        {
                            PlatformBlock newblock = new PlatformBlock() { Position = new Vector2f(xpos, ypos) };
                            _allplatformblocklist.Add(newblock);
                            _lightplatformblocklist.Add(newblock);
                        }
                        else if (Char.IsNumber(chr))
                        {
                            Door newdr = new Door((Colors)int.Parse(chr.ToString())) { Position = new Vector2f(xpos, ypos) };
                            _alldoormaplist.Add(newdr, int.Parse(chr.ToString()));
                            _shareddoormaplist.Add(newdr, int.Parse(chr.ToString()));
                        }
                        else if (chr == 's')
                        {
                            SpikeBall spike = new SpikeBall() { Position = new Vector2f(xpos, ypos) };
                            _allenitylist.Add(spike);
                            _sharedenitylist.Add(spike);
                        }
                        else if (chr == 'E')
                        {
                            SpikeBall spike = new SpikeBall() { Position = new Vector2f(xpos, ypos) };
                            _allenitylist.Add(spike);
                            _darkenitylist.Add(spike);
                        }
                        else if (chr == 'e')
                        {
                            SpikeBall spike = new SpikeBall() { Position = new Vector2f(xpos, ypos) };
                            _allenitylist.Add(spike);
                            _lightenitylist.Add(spike);
                        }
                        else if (chr == '!')
                        {
                            Door newdr = new Door(Colors.Red) { Position = new Vector2f(xpos, ypos) };
                            _alldoormaplist.Add(newdr, (int)Colors.Red);
                            _lightdoormaplist.Add(newdr, (int)Colors.Red);
                            _darkdoormaplist.Add(newdr, (int)Colors.Red);
                        }
                        else if (chr == '@')
                        {
                            Door newdr = new Door(Colors.Orange) { Position = new Vector2f(xpos, ypos) };
                            _alldoormaplist.Add(newdr, (int)Colors.Orange);
                            _lightdoormaplist.Add(newdr, (int)Colors.Orange);
                            _darkdoormaplist.Add(newdr, (int)Colors.Orange);
                        }
                        xpos += 25;
                    }
                    ypos += 15;
                }
                reader.Close();
                reader.Dispose();
                _currentplayer.Position = new Vector2f(122.5f, ypos - 65);
            }
        }

        private void CreateBorders()
        {
            //Center
            _borders.Append(new Vertex(new Vector2f(295, 0), new Color(255, 0, 0))); // Red
            _borders.Append(new Vertex(new Vector2f(305, 0), new Color(255, 0, 0))); // Red
            _borders.Append(new Vertex(new Vector2f(305, 71.4f), new Color(255, 150, 0))); // Orange
            _borders.Append(new Vertex(new Vector2f(295, 71.4f), new Color(255, 150, 0))); // Orange
            _borders.Append(new Vertex(new Vector2f(295, 71.4f), new Color(255, 150, 0))); // Orange
            _borders.Append(new Vertex(new Vector2f(305, 71.4f), new Color(255, 150, 0))); // Orange
            _borders.Append(new Vertex(new Vector2f(305, 142.8f), new Color(255, 255, 0))); // Yellow
            _borders.Append(new Vertex(new Vector2f(295, 142.8f), new Color(255, 255, 0))); // Yellow
            _borders.Append(new Vertex(new Vector2f(295, 142.8f), new Color(255, 255, 0))); // Yellow
            _borders.Append(new Vertex(new Vector2f(305, 142.8f), new Color(255, 255, 0))); // Yellow
            _borders.Append(new Vertex(new Vector2f(305, 214.2f), new Color(0, 255, 0))); // Green
            _borders.Append(new Vertex(new Vector2f(295, 214.2f), new Color(0, 255, 0))); // Green
            _borders.Append(new Vertex(new Vector2f(295, 214.2f), new Color(0, 255, 0))); // Green
            _borders.Append(new Vertex(new Vector2f(305, 214.2f), new Color(0, 255, 0))); // Green
            _borders.Append(new Vertex(new Vector2f(305, 285.6f), new Color(0, 255, 255))); // Light Blue
            _borders.Append(new Vertex(new Vector2f(295, 285.6f), new Color(0, 255, 255))); // Light Blue
            _borders.Append(new Vertex(new Vector2f(295, 285.6f), new Color(0, 255, 255))); // Light Blue
            _borders.Append(new Vertex(new Vector2f(305, 285.6f), new Color(0, 255, 255))); // Light Blue
            _borders.Append(new Vertex(new Vector2f(305, 357.0f), new Color(0, 0, 255))); // Dark Blue
            _borders.Append(new Vertex(new Vector2f(295, 357.0f), new Color(0, 0, 255))); // Dark Blue
            _borders.Append(new Vertex(new Vector2f(295, 357.0f), new Color(0, 0, 255))); // Dark Blue
            _borders.Append(new Vertex(new Vector2f(305, 357.0f), new Color(0, 0, 255))); // Dark Blue
            _borders.Append(new Vertex(new Vector2f(305, 428.4f), new Color(255, 0, 255))); // Purple
            _borders.Append(new Vertex(new Vector2f(295, 428.4f), new Color(255, 0, 255))); // Purple
            _borders.Append(new Vertex(new Vector2f(295, 428.4f), new Color(255, 0, 255))); // Purple
            _borders.Append(new Vertex(new Vector2f(305, 428.4f), new Color(255, 0, 255))); // Purple
            _borders.Append(new Vertex(new Vector2f(305, 500), new Color(255, 0, 0))); // Red
            _borders.Append(new Vertex(new Vector2f(295, 500), new Color(255, 0, 0))); // Red
            //Left
            _borders.Append(new Vertex(new Vector2f(0, 0), Color.Black));
            _borders.Append(new Vertex(new Vector2f(295, 0), Color.Black));
            _borders.Append(new Vertex(new Vector2f(295, 500), Color.Black));
            _borders.Append(new Vertex(new Vector2f(0, 500), Color.Black));
            _borders.Append(new Vertex(new Vector2f(5, 5), Color.White));
            _borders.Append(new Vertex(new Vector2f(290, 5), Color.White));
            _borders.Append(new Vertex(new Vector2f(290, 495), Color.White));
            _borders.Append(new Vertex(new Vector2f(5, 495), Color.White));
            _borders.Append(new Vertex(new Vector2f(10, 10), Color.Black));
            _borders.Append(new Vertex(new Vector2f(285, 10), Color.Black));
            _borders.Append(new Vertex(new Vector2f(285, 490), Color.Black));
            _borders.Append(new Vertex(new Vector2f(10, 490), Color.Black));
            //Right
            _borders.Append(new Vertex(new Vector2f(305, 0), Color.White));
            _borders.Append(new Vertex(new Vector2f(600, 0), Color.White));
            _borders.Append(new Vertex(new Vector2f(600, 500), Color.White));
            _borders.Append(new Vertex(new Vector2f(305, 500), Color.White));
            _borders.Append(new Vertex(new Vector2f(310, 5), Color.Black));
            _borders.Append(new Vertex(new Vector2f(595, 5), Color.Black));
            _borders.Append(new Vertex(new Vector2f(595, 495), Color.Black));
            _borders.Append(new Vertex(new Vector2f(310, 495), Color.Black));
            _borders.Append(new Vertex(new Vector2f(315, 10), Color.White));
            _borders.Append(new Vertex(new Vector2f(590, 10), Color.White));
            _borders.Append(new Vertex(new Vector2f(590, 490), Color.White));
            _borders.Append(new Vertex(new Vector2f(315, 490), Color.White));
        }

        public override void DrawObjects(RenderTarget CurrentRenderSurface, bool IsActiveScreen)
        {
            float heightoffset = -(_currentplayer.Position.Y - 300);
            CurrentRenderSurface.Draw(_borders);
            _darkside.Clear(Color.Transparent);
            _lightside.Clear(Color.Transparent);
            _darkmask.Clear(Color.Black);
            _lightmask.Clear(Color.White);
            _darkmask.Draw(new Sprite(_darkvisibility) { Scale = new Vector2f(50.0f / 512.0f, 70.0f / 512.0f), Origin = new Vector2f(256, 256), Position = new Vector2f(_currentplayer.Position.X + 15, _currentplayer.Position.Y + 15 + heightoffset) }, new RenderStates(BlendMode.None));
            _lightmask.Draw(new Sprite(_lightvisibility) { Scale = new Vector2f(50.0f / 512.0f, 70.0f / 512.0f), Origin = new Vector2f(256, 256), Position = new Vector2f(_currentplayer.Position.X + 15, _currentplayer.Position.Y + 15 + heightoffset) }, new RenderStates(BlendMode.None));
            foreach (Orb orb in _darksideorbs) { _darkmask.Draw(new Sprite(_darkvisibility) { Scale = new Vector2f(54.0f / 512.0f, 54.0f / 512.0f), Origin = new Vector2f(256, 256), Position = new Vector2f(orb.Position.X + 6, orb.Position.Y + 6 + heightoffset) }, new RenderStates(BlendMode.None)); }
            foreach (Orb orb in _lightsideorbs) { _lightmask.Draw(new Sprite(_lightvisibility) { Scale = new Vector2f(54.0f / 512.0f, 54.0f / 512.0f), Origin = new Vector2f(256, 256), Position = new Vector2f(orb.Position.X + 6, orb.Position.Y + 6 + heightoffset) }, new RenderStates(BlendMode.None)); }
            _darkmask.Display();
            _lightmask.Display();
            foreach (SolidBlock block in _darksolidblocklist) { block.Draw(_darkside, Side.DarkSide, heightoffset); }
            foreach (PlatformBlock block in _darkplatformblocklist) { block.Draw(_darkside, Side.DarkSide, heightoffset); }
            foreach (SolidBlock block in _lightsolidblocklist) { block.Draw(_lightside, Side.LightSide, heightoffset); }
            foreach (PlatformBlock block in _lightplatformblocklist) { block.Draw(_lightside, Side.LightSide, heightoffset); }
            foreach (KeyValuePair<Door, int> door in _darkdoormaplist) { door.Key.Draw(_darkside, Side.DarkSide, heightoffset); }
            foreach (KeyValuePair<Door, int> door in _lightdoormaplist) { door.Key.Draw(_lightside, Side.LightSide, heightoffset); }
            foreach (IEnity enity in _darkenitylist) { enity.Draw(_darkside, Side.DarkSide, heightoffset); }
            foreach (IEnity enity in _lightenitylist) { enity.Draw(_lightside, Side.LightSide, heightoffset); }
            _darkside.Draw(new Sprite(_darkmask.Texture));
            _lightside.Draw(new Sprite(_lightmask.Texture));
            foreach (SolidBlock block in _sharedsolidblocklist) { block.Draw(_darkside, Side.DarkSide, heightoffset); block.Draw(_lightside, Side.LightSide, heightoffset); }
            foreach (PlatformBlock block in _sharedplatformblocklist) { block.Draw(_darkside, Side.DarkSide, heightoffset); block.Draw(_lightside, Side.LightSide, heightoffset); }
            foreach (KeyValuePair<Door, int> door in _shareddoormaplist) { door.Key.Draw(_darkside, Side.DarkSide, heightoffset); door.Key.Draw(_lightside, Side.LightSide, heightoffset); }
            foreach (IEnity enity in _sharedenitylist) { enity.Draw(_darkside, Side.DarkSide, heightoffset); enity.Draw(_lightside, Side.LightSide, heightoffset); }
            foreach (Orb orb in _darksideorbs) { orb.Draw(_darkside, Side.DarkSide, heightoffset); }
            foreach (Orb orb in _lightsideorbs) { orb.Draw(_lightside, Side.LightSide, heightoffset); }
            _currentplayer.Draw(_darkside, Side.DarkSide, heightoffset);
            _currentplayer.Draw(_lightside, Side.LightSide, heightoffset);
            _darkside.Display();
            _lightside.Display();
            CurrentRenderSurface.Draw(new Sprite(_darkside.Texture) { Position = new Vector2f(10, 10) });
            CurrentRenderSurface.Draw(new Sprite(_lightside.Texture) { Position = new Vector2f(315, 10) });
            CurrentRenderSurface.Draw(_hud);
            CurrentRenderSurface.Draw(_darkorbsremainingbar);
            CurrentRenderSurface.Draw(_lightorbsremainingbar);
            CurrentRenderSurface.Draw(_darkcooldownbar);
            CurrentRenderSurface.Draw(_lightcooldownbar);
            CurrentRenderSurface.Draw(_darkpower);
            CurrentRenderSurface.Draw(_lightpower);
            if (_state == GameState.Death || _state == GameState.Paused)
            {
                CurrentRenderSurface.Draw(_hudbackground);
                CurrentRenderSurface.Draw(_hudtext);
            }
            else if (_state == GameState.FadeOut)
            {
                CurrentRenderSurface.Draw(_fadecover);
            }
        }

        public override void KeyPressed(object sender, KeyEventArgs e)
        {
            if (_state == GameState.Playing)
            {
                if (e.Code == Keyboard.Key.Escape)
                {
                    _state = GameState.Paused;
                    _hudtext.DisplayedString = "Paused";
                    _hudtext.Position = new Vector2f(300 - _hudtext.GetLocalBounds().Width / 2.0f, _hudtext.Position.Y);
                }
                else if (e.Code == Keyboard.Key.Up || e.Code == Keyboard.Key.W || e.Code == Keyboard.Key.Num8 || e.Code == Keyboard.Key.Space)
                {
                    bool changedmaps = false;
                    FloatRect currentbox = new FloatRect(_currentplayer.Position.X, _currentplayer.Position.Y - 10, 30, 50);
                    foreach (KeyValuePair<Door, int> door in _alldoormaplist)
                    {
                        if (door.Key.BoundingBox.Intersects(currentbox))
                        {
                            if (door.Value != 6)
                            {
                                _level = door.Value + 1;
                                LoadLevel();
                                changedmaps = true;
                            }
                            else
                            {
                                //Game over
                                _state = GameState.FadeOut;
                            }
                            break;
                        }
                    }
                    if (!changedmaps)
                    {
                        _moveup = true;
                    }
                }
                else if (e.Code == Keyboard.Key.Right || e.Code == Keyboard.Key.D || e.Code == Keyboard.Key.Num6)
                {
                    _moveright = true;
                }
                else if (e.Code == Keyboard.Key.Left || e.Code == Keyboard.Key.A || e.Code == Keyboard.Key.Num4)
                {
                    _moveleft = true;
                }
                else if (e.Code == Keyboard.Key.Down || e.Code == Keyboard.Key.S || e.Code == Keyboard.Key.Num5)
                {
                    _movedown = true;
                }
                else if (e.Code == Keyboard.Key.V)
                {
                    if (_cooldown == 1000)
                    {
                        if (_darkorbsremaining > 0)
                        {
                            Vector2f vel = new Vector2f(0, 0);
                            if (_moveright && !_moveleft)
                            {
                                vel.X = 4.0f;
                            }
                            else if (!_moveright && _moveleft)
                            {
                                vel.X = -4.0f;
                            }
                            if (_moveup)
                            {
                                vel.Y = -4.0f;
                            }
                            if (Math.Abs(vel.X) > 0 || Math.Abs(vel.Y) > 0)
                            {
                                _cooldown = 0;
                                _darkorbsremaining -= 1;
                                _darksideorbs.Add(new Orb() { Position = new Vector2f(_currentplayer.Position.X + 15, _currentplayer.Position.Y + 5), MoveVelocity = vel });
                            }
                        }
                    }
                }
                else if (e.Code == Keyboard.Key.B)
                {
                    if (_cooldown == 1000)
                    {
                        if (_lightorbsremaining > 0)
                        {
                            Vector2f vel = new Vector2f(0, 0);
                            if (_moveright && !_moveleft)
                            {
                                vel.X = 4.0f;
                            }
                            else if (!_moveright && _moveleft)
                            {
                                vel.X = -4.0f;
                            }
                            if (_moveup)
                            {
                                vel.Y = -4.0f;
                            }
                            if (Math.Abs(vel.X) > 0 || Math.Abs(vel.Y) > 0)
                            {
                                _cooldown = 0;
                                _lightorbsremaining -= 1;
                                _lightsideorbs.Add(new Orb() { Position = new Vector2f(_currentplayer.Position.X + 15, _currentplayer.Position.Y + 5), MoveVelocity = vel });
                            }
                        }
                    }
                }
            }
            else if (_state == GameState.Death)
            {
                if (e.Code == Keyboard.Key.Space)
                {
                    LoadLevel();
                    _state = GameState.Playing;
                }
            }
            else if (_state == GameState.Paused)
            {
                if (e.Code == Keyboard.Key.Escape)
                {
                    _state = GameState.Playing;
                }
            }
        }

        public override void KeyReleased(object sender, KeyEventArgs e)
        {
            if (_state == GameState.Playing)
            {
                if (e.Code == Keyboard.Key.Up || e.Code == Keyboard.Key.W || e.Code == Keyboard.Key.Num8)
                {
                    _moveup = false;
                }
                else if (e.Code == Keyboard.Key.Right || e.Code == Keyboard.Key.D || e.Code == Keyboard.Key.Num6)
                {
                    _moveright = false;
                }
                else if (e.Code == Keyboard.Key.Left || e.Code == Keyboard.Key.A || e.Code == Keyboard.Key.Num4)
                {
                    _moveleft = false;
                }
                else if (e.Code == Keyboard.Key.Down || e.Code == Keyboard.Key.S || e.Code == Keyboard.Key.Num5)
                {
                    _movedown = false;
                }
            }
        }

        public override void Update(int ElapsedMS, bool IsActiveScreen)
        {
            if (_isclosed && IsActiveScreen)
            {
                CloseThisScreen(this, null);
            }
            if (_state == GameState.Playing)
            {
                _cooldown = Math.Min(_cooldown + ElapsedMS, 1000);
                if (_moveup && _jumpremaining > 0)
                {
                    _jumpremaining -= ElapsedMS;
                    _movevelocity.Y = Math.Max(_movevelocity.Y - (ElapsedMS * 0.03f), -3.5f);
                }
                else
                {
                    _moveup = false;
                    _movevelocity.Y = Math.Min(_movevelocity.Y + (ElapsedMS * 0.03f), 3.5f);
                }
                if (_moveright && !_moveleft)
                {
                    _movevelocity.X = Math.Min(_movevelocity.X + (ElapsedMS * 0.03f), 2.5f);
                }
                else if (!_moveright && _moveleft)
                {
                    _movevelocity.X = Math.Max(_movevelocity.X - (ElapsedMS * 0.03f), -2.5f);
                }
                else
                {
                    if (_movevelocity.X > 0)
                    {
                        _movevelocity.X = Math.Max(_movevelocity.X - (ElapsedMS * 0.03f), 0);
                    }
                    else if (_movevelocity.X < 0)
                    {
                        _movevelocity.X = Math.Min(_movevelocity.X + (ElapsedMS * 0.03f), 0);
                    }
                }
                bool movevertical = true;
                bool movehorizontal = true;
                FloatRect currentbox = new FloatRect(_currentplayer.Position.X, _currentplayer.Position.Y - 10, 30, 50);
                FloatRect newhorizontalbox = new FloatRect(_currentplayer.Position.X + _movevelocity.X, _currentplayer.Position.Y - 10, 30, 50);
                FloatRect newverticalbox = new FloatRect(_currentplayer.Position.X, _currentplayer.Position.Y + _movevelocity.Y - 10, 30, 50);
                if (newhorizontalbox.Left < 0 || newhorizontalbox.Left + newhorizontalbox.Width > 275) { movehorizontal = false; }
                foreach (SolidBlock block in _allsolidblocklist)
                {
                    if (movehorizontal)
                    {
                        if (newhorizontalbox.Intersects(block.BoundingBox))
                        {
                            movehorizontal = false;
                        }
                    }
                    if (movevertical)
                    {
                        if (newverticalbox.Intersects(block.BoundingBox))
                        {
                            movevertical = false;
                            if (_movevelocity.Y > 0)
                            {
                                _jumpremaining = 300;
                            }
                        }
                    }
                    if (!movehorizontal && !movevertical) break;
                }
                if (movevertical && !_movedown)
                {
                    if (_movevelocity.Y > 0)
                    {
                        foreach (PlatformBlock block in _allplatformblocklist)
                        {
                            if (newverticalbox.Intersects(block.BoundingBox) && currentbox.Top + currentbox.Height < block.Position.Y)
                            {
                                movevertical = false;
                                _jumpremaining = 300;
                                break;
                            }
                        }
                    }
                }
                if (movehorizontal && movevertical)
                {
                    _currentplayer.Position = new Vector2f(_currentplayer.Position.X + _movevelocity.X, _currentplayer.Position.Y + _movevelocity.Y);
                    _currentplayer.Rotate(_movevelocity.X * 1.2f);
                }
                else if (!movehorizontal && movevertical)
                {
                    _currentplayer.Position = new Vector2f(_currentplayer.Position.X, _currentplayer.Position.Y + _movevelocity.Y);
                }
                else if (movehorizontal && !movevertical)
                {
                    _currentplayer.Position = new Vector2f(_currentplayer.Position.X + _movevelocity.X, _currentplayer.Position.Y);
                    _currentplayer.Rotate(_movevelocity.X * 1.2f);
                }
                bool died = false;
                foreach (IEnity enity in _allenitylist)
                {
                    FloatRect enityrect = enity.BoundingBox;
                    if ((movehorizontal && enityrect.Intersects(newhorizontalbox)) || (movevertical && enityrect.Intersects(newverticalbox)))
                    {
                        died = true;
                        break;
                    }
                }
                if (died)
                {
                    _state = GameState.Death;
                    _hudtext.DisplayedString = "You Died";
                    _hudtext.Position = new Vector2f(300 - _hudtext.GetLocalBounds().Width / 2.0f, _hudtext.Position.Y);
                }
                for (int i = 0; i < _darksideorbs.Count; )
                {
                    _darksideorbs[i].LiveTime += ElapsedMS;
                    if (_darksideorbs[i].LiveTime < 1500)
                    {
                        _darksideorbs[i].Position = new Vector2f(_darksideorbs[i].Position.X + _darksideorbs[i].MoveVelocity.X, _darksideorbs[i].Position.Y + _darksideorbs[i].MoveVelocity.Y);
                        i++;
                    }
                    else
                    {
                        _darksideorbs.RemoveAt(i);
                    }
                }
                for (int i = 0; i < _lightsideorbs.Count; )
                {
                    _lightsideorbs[i].LiveTime += ElapsedMS;
                    if (_lightsideorbs[i].LiveTime < 1500)
                    {
                        _lightsideorbs[i].Position = new Vector2f(_lightsideorbs[i].Position.X + _lightsideorbs[i].MoveVelocity.X, _lightsideorbs[i].Position.Y + _lightsideorbs[i].MoveVelocity.Y);
                        i++;
                    }
                    else
                    {
                        _lightsideorbs.RemoveAt(i);
                    }
                }
                _darkorbsremainingbar.Size = new Vector2f((float)_darkorbsremaining / 10.0f * 100.0f, _darkorbsremainingbar.Size.Y);
                _lightorbsremainingbar.Size = new Vector2f((float)_lightorbsremaining / 10.0f * 100.0f, _lightorbsremainingbar.Size.Y);
                _darkcooldownbar.Size = new Vector2f((float)_cooldown / 1000.0f * 100.0f, _darkcooldownbar.Size.Y);
                _lightcooldownbar.Size = new Vector2f((float)_cooldown / 1000.0f * 100.0f, _lightcooldownbar.Size.Y);
            }
            else if (_state == GameState.FadeOut)
            {
                _fadealpha = Math.Min(255, _fadealpha + (ElapsedMS * 0.2f));
                _fadecover.FillColor = new Color(0, 0, 0, (byte)_fadealpha);
                if (IsActiveScreen && !_isclosed)
                {
                    if (_fadealpha == 255)
                    {
                        EndingScreen endscreen = new EndingScreen();
                        SwitchScreen(this, new SwitchScreenArgs(endscreen));
                        _isclosed = true;
                    }
                }
            }
        }
    }
}
