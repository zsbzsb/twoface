﻿using System;

namespace TwoFace
{
    public enum Side
    {
        DarkSide,
        LightSide
    }
    public enum IntroductionState
    {
        BlackWaiting,
        Flashing,
        TextFadeIn,
        TextSolid,
        TextFadeOut,
        FadeOut
    }
    public enum EndState
    {
        BlackWaiting,
        Flashing,
        TextFadeIn,
        TextSolid,
        TextFadeOut,
        DogShow
    }
    public enum Colors
    {
        Red = 0,
        Orange = 1,
        Yellow = 2,
        Green = 3,
        LightBlue = 4,
        DarkBlue = 5,
        Purple = 6
    }
    public enum GameState
    {
        Playing,
        Death,
        Paused,
        FadeOut
    }
}
