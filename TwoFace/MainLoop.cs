﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    public class MainLoop
    {
        private const int fixedtimestep = 15;
        RenderWindow _window = null;
        private List<Keyboard.Key> _downkeylist = new List<Keyboard.Key>();
        private Stopwatch elapsedtimer = null;
        private long elapsedtime = 0;
        private List<IScreenManager> screencontrollerlist = new List<IScreenManager>();
        public void Run()
        {
            _window = new RenderWindow(new VideoMode(600, 500), "TwoFace", Styles.Close);
            _window.SetFramerateLimit(60);
            _window.SetVerticalSyncEnabled(false);
            _window.SetKeyRepeatEnabled(false);
            BindWindowEvents();
            ResourceLoader.LoadResources();
            screencontrollerlist.Add(new MainMenu());
            screencontrollerlist[screencontrollerlist.Count - 1].CloseThisScreen += CloseCurrentScreenManager;
            screencontrollerlist[screencontrollerlist.Count - 1].SwitchScreen += SwitchScreenManagers;
            elapsedtimer = Stopwatch.StartNew();
            while (_window.IsOpen())
            {
                elapsedtime += elapsedtimer.ElapsedMilliseconds;
                elapsedtimer.Reset();
                elapsedtimer.Start();
                _window.DispatchEvents();
                _window.Clear(Color.Black);
                while (elapsedtime >= fixedtimestep)
                {
                    elapsedtime -= fixedtimestep;
                    for (int c = 0; c < screencontrollerlist.Count; c++)
                    {
                        bool isactive = (c == (screencontrollerlist.Count - 1));
                        int count = screencontrollerlist.Count;
                        screencontrollerlist[c].Update(fixedtimestep, isactive);
                        if (count > screencontrollerlist.Count)
                        {
                            c -= 1;
                        }
                        else
                        {
                            screencontrollerlist[c].UpdateObjects(fixedtimestep, isactive);
                            screencontrollerlist[c].UpdateFrames(fixedtimestep, isactive);
                        }
                    }
                }
                for (int c = 0; c < screencontrollerlist.Count; c++)
                {
                    bool isactive = (c == (screencontrollerlist.Count - 1));
                    if (isactive) screencontrollerlist[c].DrawObjects(_window, isactive);
                    else if (screencontrollerlist[c].DrawWhenInactive) screencontrollerlist[c].DrawObjects(_window, isactive);
                }
                _window.Display();
            }
        }
        private void BindWindowEvents()
        {
            _window.Closed += WindowClose;
            _window.KeyPressed += KeyPressed;
            _window.KeyReleased += KeyReleased;
            _window.MouseMoved += MouseMove;
            _window.MouseButtonPressed += MouseDown;
            _window.MouseButtonReleased += MouseUp;
        }
        private void WindowClose(object sender, EventArgs e)
        {
            _window.Close();
        }
        private void MouseMove(object sender, MouseMoveEventArgs e)
        {
            Vector2f convertedcords = _window.MapPixelToCoords(new Vector2i(e.X, e.Y));
            e.X = (int)convertedcords.X;
            e.Y = (int)convertedcords.Y;
            screencontrollerlist[screencontrollerlist.Count - 1].MouseMove(sender, e);
        }
        private void MouseDown(object sender, MouseButtonEventArgs e)
        {
            Vector2f convertedcords = _window.MapPixelToCoords(new Vector2i(e.X, e.Y));
            e.X = (int)convertedcords.X;
            e.Y = (int)convertedcords.Y;
            screencontrollerlist[screencontrollerlist.Count - 1].MouseDown(sender, e);
        }
        private void MouseUp(object sender, MouseButtonEventArgs e)
        {
            Vector2f convertedcords = _window.MapPixelToCoords(new Vector2i(e.X, e.Y));
            e.X = (int)convertedcords.X;
            e.Y = (int)convertedcords.Y;
            screencontrollerlist[screencontrollerlist.Count - 1].MouseUp(sender, e);
        }
        private void KeyPressed(object sender, KeyEventArgs e)
        {
            if (_downkeylist.Contains(e.Code) == false)
            {
                _downkeylist.Add(e.Code);
                screencontrollerlist[screencontrollerlist.Count - 1].KeyPressed(sender, e);
            }
        }
        private void KeyReleased(object sender, KeyEventArgs e)
        {
            _downkeylist.Remove(e.Code);
            screencontrollerlist[screencontrollerlist.Count - 1].KeyReleased(sender, e);
        }
        private void CloseCurrentScreenManager(object sender, EventArgs e)
        {
            screencontrollerlist[screencontrollerlist.Count - 1].CloseThisScreen -= CloseCurrentScreenManager;
            screencontrollerlist[screencontrollerlist.Count - 1].SwitchScreen -= SwitchScreenManagers;
            screencontrollerlist.RemoveAt(screencontrollerlist.Count - 1);
            if (screencontrollerlist.Count == 0) _window.Close();
            else
            {
                screencontrollerlist[screencontrollerlist.Count - 1].CloseThisScreen += CloseCurrentScreenManager;
                screencontrollerlist[screencontrollerlist.Count - 1].SwitchScreen += SwitchScreenManagers;
            }
        }
        private void SwitchScreenManagers(object sender, SwitchScreenArgs e)
        {
            screencontrollerlist[screencontrollerlist.Count - 1].CloseThisScreen -= CloseCurrentScreenManager;
            screencontrollerlist[screencontrollerlist.Count - 1].SwitchScreen -= SwitchScreenManagers;
            screencontrollerlist.Add(e.CurrentScreenManager);
            screencontrollerlist[screencontrollerlist.Count - 1].CloseThisScreen += CloseCurrentScreenManager;
            screencontrollerlist[screencontrollerlist.Count - 1].SwitchScreen += SwitchScreenManagers;
        }
    }
}
