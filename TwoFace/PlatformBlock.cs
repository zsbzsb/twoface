﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    public class PlatformBlock
    {
        private VertexArray _outerbackground = new VertexArray(PrimitiveType.Quads);
        private VertexArray _innerbackground = new VertexArray(PrimitiveType.Quads);
        private VertexArray _lines = new VertexArray(PrimitiveType.Lines);
        private Vector2f _position = new Vector2f(0, 0);

        public Vector2f Position { get { return _position; } set { _position = value; } }
        public FloatRect BoundingBox { get { return new FloatRect(_position.X, _position.Y, 25, 15); } }

        public PlatformBlock()
        {
            _outerbackground.Append(new Vertex(new Vector2f(0, 0)));
            _outerbackground.Append(new Vertex(new Vector2f(25, 0)));
            _outerbackground.Append(new Vertex(new Vector2f(25, 15)));
            _outerbackground.Append(new Vertex(new Vector2f(0, 15)));
            _innerbackground.Append(new Vertex(new Vector2f(2, 2)));
            _innerbackground.Append(new Vertex(new Vector2f(23, 2)));
            _innerbackground.Append(new Vertex(new Vector2f(23, 13)));
            _innerbackground.Append(new Vertex(new Vector2f(2, 13)));
            _lines.Append(new Vertex(new Vector2f(5, 2)));
            _lines.Append(new Vertex(new Vector2f(5, 13)));
            _lines.Append(new Vertex(new Vector2f(12, 2)));
            _lines.Append(new Vertex(new Vector2f(12, 13)));
            _lines.Append(new Vertex(new Vector2f(20, 2)));
            _lines.Append(new Vertex(new Vector2f(20, 13)));
        }

        public void Draw(RenderTarget DrawTarget, Side CurrentSide, float HeightOffset)
        {
            RenderStates states = RenderStates.Default;
            Transform transform = states.Transform;
            transform.Translate(_position.X, _position.Y + HeightOffset);
            states.Transform = transform;
            if (CurrentSide == Side.LightSide)
            {
                for (uint i = 0; i < _outerbackground.VertexCount; i++) { _outerbackground[i] = new Vertex(_outerbackground[i].Position, Color.Black); }
                for (uint i = 0; i < _innerbackground.VertexCount; i++) { _innerbackground[i] = new Vertex(_innerbackground[i].Position, Color.White); }
                for (uint i = 0; i < _lines.VertexCount; i++) { _lines[i] = new Vertex(_lines[i].Position, Color.Black); }
            }
            else if (CurrentSide == Side.DarkSide)
            {
                for (uint i = 0; i < _outerbackground.VertexCount; i++) { _outerbackground[i] = new Vertex(_outerbackground[i].Position, Color.White); }
                for (uint i = 0; i < _innerbackground.VertexCount; i++) { _innerbackground[i] = new Vertex(_innerbackground[i].Position, Color.Black); }
                for (uint i = 0; i < _lines.VertexCount; i++) { _lines[i] = new Vertex(_lines[i].Position, Color.White); }
            }
            DrawTarget.Draw(_outerbackground, states);
            DrawTarget.Draw(_innerbackground, states);
            DrawTarget.Draw(_lines, states);
        }
    }
}
