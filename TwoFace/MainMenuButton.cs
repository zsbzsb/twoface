﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace TwoFace
{
    public class MainMenuButton
    {
        private Text _displaytext = new Text("", ResourceLoader.MainGameFont) { Color = Color.Black, CharacterSize = 24 };
        private VertexArray _background = new VertexArray(PrimitiveType.Quads);
        private Vector2f _position = new Vector2f(0, 0);

        public string DisplayText { get { return _displaytext.DisplayedString; } set { _displaytext.DisplayedString = value; } }
        public Vector2f Position { get { return _position; } set { _position = value; } }

        public MainMenuButton()
        {
            _background.Append(new Vertex(new Vector2f(0, 0), Color.White));
            _background.Append(new Vertex(new Vector2f(150, 0), Color.White));
            _background.Append(new Vertex(new Vector2f(150, 40), Color.White));
            _background.Append(new Vertex(new Vector2f(0, 40), Color.White));
            _background.Append(new Vertex(new Vector2f(3, 3), Color.Black));
            _background.Append(new Vertex(new Vector2f(147, 3), Color.Black));
            _background.Append(new Vertex(new Vector2f(147, 37), Color.Black));
            _background.Append(new Vertex(new Vector2f(3, 37), Color.Black));
            _background.Append(new Vertex(new Vector2f(5, 5), Color.White));
            _background.Append(new Vertex(new Vector2f(145, 5), Color.White));
            _background.Append(new Vertex(new Vector2f(145, 35), Color.White));
            _background.Append(new Vertex(new Vector2f(5, 35), Color.White));
        }

        public void Draw(RenderTarget DrawTarget)
        {
            RenderStates states = RenderStates.Default;
            Transform transform = states.Transform;
            transform.Translate(_position);
            states.Transform = transform;
            DrawTarget.Draw(_background, states);
            _displaytext.Position = new Vector2f(75 - (_displaytext.GetLocalBounds().Width / 2.0f), 15 - (_displaytext.GetLocalBounds().Height / 2.0f));
            DrawTarget.Draw(_displaytext, states);
        }

        public bool PointinButton(Vector2i Point)
        {
            return new FloatRect(_position.X, _position.Y, 150, 40).Contains(Point.X, Point.Y);
        }

    }
}
